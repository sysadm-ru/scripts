#!/bin/bash -x
# Description:L2TP/IPsec for CentOS 6.5 64bit
# 2014/01/06 @ysaotome
# 2014/01/20 @The_ru

(
### setting
/bin/cat << _SECRETS_ > /tmp/SECRETS_TMP.txt
#==============================================
# username auth_server password auth_ipaddress
username1 pptpd userpass1 *
username2 pptpd userpass2 *
#==============================================
_SECRETS_

PSK_SECRETS='HOGESECRETS'

COLOR_LIGHT_GREEN='\033[1;32m'
COLOR_LIGHT_BLUE='\033[1;34m'
COLOR_YELLOW='\033[1;33m'
COLOR_RED='\033[0;31m'
COLOR_WHITE='\033[1;37m'
COLOR_DEFAULT='\033[0m'

IPADDR_GLOBAL=$(/sbin/ip addr show eth0 2>/dev/null | /bin/grep 'inet ' | /bin/sed -e 's/.*inet \([^ ]*\)\/.*/\1/')

VPN_LOCAL_IPADDRESS='10.0.0.1'
VPN_REMOTE_IPADDRESS='10.0.0.100-200'

ARC=$(/bin/uname -m)

### setup

## SELINUX
echo "(石川さんごめんなさい)"
/usr/sbin/setenforce 0
/bin/sed -i.org -e 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

## IP_FORWARD
/bin/echo '1' > /proc/sys/net/ipv4/ip_forward
/bin/sed -i.org -e 's/net.ipv4.ip_forward = 0/net.ipv4.ip_forward = 1/g' /etc/sysctl.conf
/bin/cat << _SYSCTLCONF_ >> /etc/sysctl.conf
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.conf.lo.accept_redirects = 0
net.ipv4.conf.lo.send_redirects = 0
net.ipv4.conf.eth0.accept_redirects = 0
net.ipv4.conf.eth0.send_redirects = 0
_SYSCTLCONF_
/sbin/sysctl -p /etc/sysctl.conf

## EPEL
/bin/rpm --import http://ftp.riken.jp/Linux/fedora/epel/RPM-GPG-KEY-EPEL
/bin/rpm -ivh http://ftp.riken.jp/Linux/fedora/epel/6/${ARC}/epel-release-6-8.noarch.rpm
/bin/sed -i.org -e "s/enabled.*=.*1/enabled=0/g" /etc/yum.repos.d/epel.repo 

## 
/usr/bin/yum -y --enablerepo=epel install xl2tpd openswan lsof
/usr/bin/yum -y update

## L2TP
/bin/sed -i.org -e "s/; listen-addr.*/listen-addr = ${IPADDR_GLOBAL}/g" -e "s/ip range.*/ip range = ${VPN_REMOTE_IPADDRESS}/g" -e "s/local ip.*/local ip = ${VPN_LOCAL_IPADDRESS}/g" /etc/xl2tpd/xl2tpd.conf

/bin/sed -i.org -e "s/^ms-dns/# ms-dns/g" -e "s/^noccp/# noccp/g" /etc/ppp/options.xl2tpd
/bin/cat << _XL2TPDCONF_ >> /etc/ppp/options.xl2tpd
ms-dns 8.8.8.8
ms-dns 209.244.0.3
ms-dns 208.67.222.222
name xl2tpd
refuse-pap
refuse-chap
refuse-mschap
require-mschap-v2
persist
logfile /var/log/xl2tpd.log
_XL2TPDCONF_

## IPsec
/bin/sed -i.org -e "s/^#include/include/g" /etc/ipsec.conf
/bin/cat << _IPSECCONF_ > /etc/ipsec.d/l2tp-ipsec.conf
conn L2TP-PSK-NAT
    rightsubnet=0.0.0.0/0
    dpddelay=10
    dpdtimeout=20
    dpdaction=clear
    forceencaps=yes
    also=L2TP-PSK-noNAT
conn L2TP-PSK-noNAT
    authby=secret
    pfs=no
    auto=add
    keyingtries=3
    rekey=no
    ikelifetime=8h
    keylife=1h
    type=transport
    left=${IPADDR_GLOBAL}
    leftprotoport=17/1701
    right=%any
    rightprotoport=17/%any
_IPSECCONF_

/bin/cat /tmp/SECRETS_TMP.txt >> /etc/ppp/chap-secrets
/bin/rm /tmp/SECRETS_TMP.txt
/bin/echo -e ": PSK \"${PSK_SECRETS}\"" > /etc/ipsec.d/default.secrets

## IPTABLES
/bin/echo -e '/sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE' >> /etc/rc.local
/sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
/sbin/iptables -A INPUT -i eth0 -p udp -m udp --dport 1701 -j ACCEPT
/sbin/iptables -A INPUT -i eth0 -p udp -m udp --dport 500 -j ACCEPT
/sbin/iptables -A INPUT -i eth0 -p udp -m udp --dport 4500 -j ACCEPT
/sbin/iptables -A INPUT -i eth0 -p esp -j ACCEPT
/sbin/iptables -D FORWARD -j DROP
/sbin/iptables -A FORWARD -i ppp+ -o eth0 -j ACCEPT
/sbin/iptables -A FORWARD -i eth0 -o ppp+ -j ACCEPT
/sbin/iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

##
/sbin/service iptables save
/sbin/service iptables restart
/sbin/chkconfig iptables on
/sbin/service xl2tpd start
/sbin/chkconfig xl2tpd on
/sbin/service ipsec start
/sbin/chkconfig ipsec on


### Finish
/bin/echo -e "${COLOR_WHITE}L2TP/IPsec SERVER IP     : ${COLOR_LIGHT_GREEN}${IPADDR_GLOBAL}${COLOR_DEFAULT}"
/bin/echo -e "${COLOR_WHITE}L2TP/IPsec USER/PASSWORD : \n${COLOR_LIGHT_GREEN}$(/bin/cat /etc/ppp/chap-secrets)${COLOR_DEFAULT}"
/bin/echo -e "${COLOR_WHITE}L2TP/IPsec PSK SECRETS   : ${COLOR_LIGHT_GREEN}${PSK_SECRETS}${COLOR_DEFAULT}"
/bin/echo -e "${COLOR_WHITE}Install log              : ${COLOR_LIGHT_GREEN}/var/log/L2TP_IPsec-installer.log${COLOR_DEFAULT}"
) 2>&1 | /usr/bin/tee /var/log/L2TP_IPsec-installer.log


