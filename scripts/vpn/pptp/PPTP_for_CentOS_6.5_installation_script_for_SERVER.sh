#!/bin/bash -x
# Description:PPTP for CentOS 6.5 64bit
# 2013/07/28 @ysaotome
# 2014/01/20 @The_ru
 
(
### setting
/bin/cat << _SECRETS_ > /tmp/SECRETS_TMP.txt
#==============================================\n
# username auth_server password auth_ipaddress\n
username1 pptpd userpass1 *
username2 pptpd userpass2 *
#==============================================\n
_SECRETS_

COLOR_LIGHT_GREEN='\033[1;32m'
COLOR_LIGHT_BLUE='\033[1;34m'
COLOR_YELLOW='\033[1;33m'
COLOR_RED='\033[0;31m'
COLOR_WHITE='\033[1;37m'
COLOR_DEFAULT='\033[0m'

IPADDR_GLOBAL=$(/sbin/ip addr show eth0 2>/dev/null | /bin/grep 'inet ' | /bin/sed -e 's/.*inet \([^ ]*\)\/.*/\1/')

VPN_LOCAL_IPADDRESS='10.0.0.1'
VPN_REMOTE_IPADDRESS='10.0.0.100-200'

ARC=$(/bin/uname -m)

### SETUP

## SELINUX 
/usr/sbin/setenforce 0
/bin/sed -i.org -e 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

## IP_FORWARD 
/bin/echo '1' > /proc/sys/net/ipv4/ip_forward
/bin/sed -i.orig -e 's/net.ipv4.ip_forward = 0/net.ipv4.ip_forward = 1/g' /etc/sysctl.conf
/sbin/sysctl -p /etc/sysctl.conf

## 
/bin/rpm -Uvh http://poptop.sourceforge.net/yum/stable/rhel6/pptp-release-current.noarch.rpm

## 
/usr/bin/yum -y install ppp pptpd
/usr/bin/yum -y update

## PPTP
/bin/echo -e 'localip '${VPN_LOCAL_IPADDRESS}'\nremoteip '${VPN_REMOTE_IPADDRESS} >> /etc/pptpd.conf
/bin/echo -e 'ms-dns 8.8.8.8\nms-dns 209.244.0.3\nms-dns 208.67.222.222' >> /etc/ppp/options.pptpd
/bin/cat /tmp/SECRETS_TMP.txt >> /etc/ppp/chap-secrets
/bin/rm /tmp/SECRETS_TMP.txt

## IPTABLES
/bin/echo -e '/sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE' >> /etc/rc.local
/sbin/iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
/sbin/iptables -A INPUT -i eth0 -p tcp --dport 1723 -j ACCEPT
/sbin/iptables -A INPUT -i eth0 -p gre -j ACCEPT
/sbin/iptables -D FORWARD -j DROP
/sbin/iptables -A FORWARD -i ppp+ -o eth0 -j ACCEPT
/sbin/iptables -A FORWARD -i eth0 -o ppp+ -j ACCEPT
/sbin/iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

## 
/sbin/service iptables save
/sbin/service iptables restart
/sbin/chkconfig iptables on
/sbin/service pptpd restart
/sbin/chkconfig pptpd on


### Finish
/bin/echo -e "${COLOR_WHITE}PPTP SERVER IP     : ${COLOR_LIGHT_GREEN}${IPADDR_GLOBAL}${COLOR_DEFAULT}"
/bin/echo -e "${COLOR_WHITE}PPTP USER/PASSWORD : \n${COLOR_LIGHT_GREEN}$(/bin/cat /etc/ppp/chap-secrets)${COLOR_DEFAULT}"
/bin/echo -e "${COLOR_WHITE}Install log        : ${COLOR_LIGHT_GREEN}/var/log/vpn-installer.log${COLOR_DEFAULT}"
) 2>&1 | /usr/bin/tee /var/log/pptp-installer.log


